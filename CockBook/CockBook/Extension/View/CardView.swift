//
//  CardView.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/22/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable

extension UIView{
    func cardView()  {
        self.layer.cornerRadius = 10.0
        self.layer.shadowColor = UIColor.gray.cgColor
        //        self.layer.borderColor = (UIColor.deepGrey.cgColor)
        //        self.layer.borderWidth = 1
        
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        self.layer.shadowOpacity = 0.4
    }

}
