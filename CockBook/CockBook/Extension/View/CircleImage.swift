//
//  CircleImage.swift
//  AlamofireTest
//
//  Created by Ta Tuan Macbook on 3/20/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

extension UIImageView{
    func circleImage()  {
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
    }
    func borderImage()  {
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
    }
    
}
