//
//  PresenterDetailFood.swift
//  CockBook
//
//  Created by Tinh Van on 4/11/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

protocol DetailFoodDelegate:class {
    func getDataDetailFood()
}
class PresenterDetailFood{
    weak var delegateDetailFood : DetailFoodDelegate?
    
    convenience init(delegate: DetailFoodDelegate){
        self.init()
        self.delegateDetailFood = delegate
    }
    var foodName:String?
    func getNameOfFood(name : String){
        self.foodName = name
    }
    
    let detailFoodProvider = MoyaProvider<DetailFoodApi>()
    
    var detaiFoodModel : ModelDetailFood?

    func getDataForDetailFood(){
        detailFoodProvider.request(DetailFoodApi.foodKey(nameOfFood: foodName ?? "")) { (result) in
            switch result {
            case .success(let response):
                do{
                    let json = try response.mapJSON()
                    guard let dataJSON = Mapper<ModelDetailFood>().map(JSONObject: json)
                        else{
                            return
                    }
                    self.detaiFoodModel = dataJSON
                    self.delegateDetailFood?.getDataDetailFood()
                    
                }catch{
                    print("error")
                }
            case .failure(_):
                print("error")
            }
        }
        
        
        
    }
    

}
