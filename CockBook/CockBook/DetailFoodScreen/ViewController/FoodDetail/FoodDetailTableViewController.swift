//
//  FoodDetailTableViewController.swift
//  CockBook
//
//  Created by Tinh Van on 4/16/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class FoodDetailTableViewController: UITableViewController {
    fileprivate let presenterDetailFood = PresenterDetailFood()
    
    fileprivate let headerID = "headerID"
    fileprivate let detailFoodCell = "DetailFoodCell"
    fileprivate let ingredientFoodCell = "IngredientFoodCell"
    fileprivate let videoTutorialCell = "VideoTutorialCell"
    
    fileprivate let padding:CGFloat = 16
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        presenterDetailFood.delegateDetailFood = self
        presenterDetailFood.getDataForDetailFood()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Register cell classes
        tableView.register(HeaderView.self, forCellReuseIdentifier: headerID)
        tableView.register(DetailFoodCell.self, forCellReuseIdentifier: detailFoodCell)
        tableView.register(IngredientFoodCell.self, forCellReuseIdentifier: ingredientFoodCell)
        tableView.register(VideoTutorialCell.self, forCellReuseIdentifier: videoTutorialCell)
        
        navigationItem.title = presenterDetailFood.foodName?.uppercased()
        
        if self.isMovingFromParent {
            print("back button")
        }
        
    }
    
    func getFoodName(foodName:String){
        presenterDetailFood.getNameOfFood(name: foodName)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        if indexPath.row == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: headerID, for: indexPath) as! HeaderView
            headerCell.setImageForHeader(imgHeaderLink: presenterDetailFood.detaiFoodModel?.meals?[0].strMealThumb ?? "")
            return headerCell
            
        }else if indexPath.row == 1 {
            let detailCell = tableView.dequeueReusableCell(withIdentifier: detailFoodCell, for: indexPath) as! DetailFoodCell
            detailCell.setDetail(strIn: presenterDetailFood.detaiFoodModel?.meals?[0].strInstructions ?? "" )
            return detailCell
            
        }else if indexPath.row == 2 {
            let ingredientCell = tableView.dequeueReusableCell(withIdentifier: ingredientFoodCell, for: indexPath) as! IngredientFoodCell
            ingredientCell.setDataIngredient(
                strIngredient1: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient1, strMeasure1: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure1,
                strIngredient2: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient2, strMeasure2: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure2,
                strIngredient3: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient3, strMeasure3: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure3,
                strIngredient4: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient4, strMeasure4: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure4,
                strIngredient5: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient5, strMeasure5: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure5,
                strIngredient6: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient6, strMeasure6: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure6,
                strIngredient7: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient7, strMeasure7: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure7,
                strIngredient8: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient8, strMeasure8: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure8,
                strIngredient9: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient9, strMeasure9: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure9,
                strIngredient10: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient10, strMeasure10: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure10,
                strIngredient11: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient11, strMeasure11: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure11,
                strIngredient12: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient12, strMeasure12: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure12,
                strIngredient13: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient13, strMeasure13: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure12,
                strIngredient14: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient14, strMeasure14: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure14,
                strIngredient15: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient15, strMeasure15: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure15,
                strIngredient16: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient16, strMeasure16: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure16,
                strIngredient17: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient17, strMeasure17: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure17,
                strIngredient18: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient18, strMeasure18: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure18,
                strIngredient19: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient19, strMeasure19: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure19,
                strIngredient20: presenterDetailFood.detaiFoodModel?.meals?[0].strMeasure20, strMeasure20: presenterDetailFood.detaiFoodModel?.meals?[0].strIngredient20)
            return ingredientCell
        }
        
        let videoCell = tableView.dequeueReusableCell(withIdentifier: videoTutorialCell, for: indexPath) as! VideoTutorialCell
        videoCell.setDataForVideoView(strLink: presenterDetailFood.detaiFoodModel?.meals?[0].strYoutube)
        return videoCell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension FoodDetailTableViewController: DetailFoodDelegate{
    func getDataDetailFood() {
        tableView.reloadData()
    }
}

