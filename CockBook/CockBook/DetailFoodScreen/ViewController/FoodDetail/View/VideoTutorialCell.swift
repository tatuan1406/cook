//
//  VideoTutorialCell.swift
//  CockBook
//
//  Created by Tinh Van on 4/12/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import YouTubePlayer

class VideoTutorialCell: UITableViewCell, YouTubePlayerDelegate{
    
    let videoView: YouTubePlayerView = {
        let view = YouTubePlayerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(videoView)
        let view = ["videoView": videoView]
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[videoView]-10-|", options: [], metrics: nil, views: view))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[videoView(450)]-10-|", options: [], metrics: nil, views: view))
    }
    
    func setDataForVideoView(strLink:String?){
        let myVideoURL = NSURL(string: strLink ?? "link")
        videoView.frame = self.frame
        videoView.loadVideoURL(myVideoURL! as URL)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

