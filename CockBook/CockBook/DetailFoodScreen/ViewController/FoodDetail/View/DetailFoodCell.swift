//
//  DetailFoodCell.swift
//  CockBook
//
//  Created by Tinh Van on 4/12/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import UIKit

class DetailFoodCell: UITableViewCell{
    let bgView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cardView()
        view.backgroundColor = .white
        return view
    }()
    
    let title: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "DETAIL"
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 21)
        lbl.numberOfLines = 1
        lbl.textColor = UIColor.deepGreen
        lbl.textAlignment = .center
        return lbl
    }()
    
    let lblDetail : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .justified
        lbl.numberOfLines = 0
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style:style, reuseIdentifier:reuseIdentifier)
        
        contentView.addSubview(bgView)
        bgView.addSubview(title)
        bgView.addSubview(lblDetail)
        
        let detailViews = ["bgView":bgView, "title":title, "lblDetail":lblDetail]
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[bgView]-10-|", options: [], metrics: nil, views: detailViews))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[bgView]-10-|", options: [], metrics: nil, views: detailViews))
        
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[title]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[lblDetail]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[title(25)]-8-[lblDetail]-8-|", options: [], metrics: nil, views: detailViews))
    }
    
    func setDetail(strIn :String? ){
        lblDetail.text = strIn
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
