//
//  IngredientFoodCell.swift
//  CockBook
//
//  Created by Tinh Van on 4/12/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class IngredientFoodCell: UITableViewCell{
    
    let bgView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cardView()
        view.backgroundColor = .white
        return view
    }()
    
    let title: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "INGREDIENT"
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 21)
        lbl.numberOfLines = 1
        lbl.textColor = UIColor.deepGreen
        lbl.textAlignment = .center
        return lbl
    }()
    
    let stack1 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient1 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure1 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    
    //
    let stack2 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient2 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure2 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack3 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient3 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure3 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack4 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient4 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure4 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack5 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient5 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure5 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack6 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient6 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure6 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack7 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient7 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure7 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack8 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient8 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure8 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack9 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient9 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure9 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack10 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient10 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure10 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack11 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient11 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure11 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack12 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient12 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure12 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack13 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient13 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure13 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack14 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient14 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure14: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack15 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient15 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl 
    }()
    
    let lblMeasure15 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack16 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient16 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure16 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack17 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient17 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure17 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack18 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient18 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure18 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack19 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient19 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure19 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    //
    let stack20 : UIStackView = {
        let st = UIStackView()
        st.backgroundColor = .white
        st.translatesAutoresizingMaskIntoConstraints = false
        st.axis = .horizontal
        st.alignment = .fill
        st.distribution = .fill
        st.spacing = 10
        return st
    }()
    
    let lblIngredient20 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = R.string.localizable.ingredient()
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let lblMeasure20 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont(name: "HelveticaNeue", size: 18)
        lbl.text = R.string.localizable.measure()
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        contentView.addSubview(bgView)
        bgView.addSubview(title)
        //1
        bgView.addSubview(stack1)
        stack1.addArrangedSubview(lblIngredient1)
        stack1.addArrangedSubview(lblMeasure1)
        //2
        bgView.addSubview(stack2)
        stack2.addArrangedSubview(lblIngredient2)
        stack2.addArrangedSubview(lblMeasure2)
        //2
        bgView.addSubview(stack3)
        stack3.addArrangedSubview(lblIngredient3)
        stack3.addArrangedSubview(lblMeasure3)
        //2
        bgView.addSubview(stack4)
        stack4.addArrangedSubview(lblIngredient4)
        stack4.addArrangedSubview(lblMeasure4)
        //2
        bgView.addSubview(stack5)
        stack5.addArrangedSubview(lblIngredient5)
        stack5.addArrangedSubview(lblMeasure5)
        //2
        bgView.addSubview(stack6)
        stack6.addArrangedSubview(lblIngredient6)
        stack6.addArrangedSubview(lblMeasure6)
        //2
        bgView.addSubview(stack7)
        stack7.addArrangedSubview(lblIngredient7)
        stack7.addArrangedSubview(lblMeasure7)
        //2
        bgView.addSubview(stack8)
        stack8.addArrangedSubview(lblIngredient8)
        stack8.addArrangedSubview(lblMeasure8)
        //2
        bgView.addSubview(stack9)
        stack9.addArrangedSubview(lblIngredient9)
        stack9.addArrangedSubview(lblMeasure9)
        //2
        bgView.addSubview(stack10)
        stack10.addArrangedSubview(lblIngredient10)
        stack10.addArrangedSubview(lblMeasure10)
        //2
        bgView.addSubview(stack11)
        stack11.addArrangedSubview(lblIngredient11)
        stack11.addArrangedSubview(lblMeasure11)
        //2
        bgView.addSubview(stack12)
        stack12.addArrangedSubview(lblIngredient12)
        stack12.addArrangedSubview(lblMeasure12)
        //2
        bgView.addSubview(stack13)
        stack13.addArrangedSubview(lblIngredient13)
        stack13.addArrangedSubview(lblMeasure13)
        //2
        bgView.addSubview(stack14)
        stack14.addArrangedSubview(lblIngredient14)
        stack14.addArrangedSubview(lblMeasure14)
        //2
        bgView.addSubview(stack15)
        stack15.addArrangedSubview(lblIngredient15)
        stack15.addArrangedSubview(lblMeasure15)
        //2
        bgView.addSubview(stack16)
        stack16.addArrangedSubview(lblIngredient16)
        stack16.addArrangedSubview(lblMeasure16)
        //2
        bgView.addSubview(stack17)
        stack17.addArrangedSubview(lblIngredient17)
        stack17.addArrangedSubview(lblMeasure17)
        //2
        bgView.addSubview(stack18)
        stack18.addArrangedSubview(lblIngredient18)
        stack18.addArrangedSubview(lblMeasure18)
        //2
        bgView.addSubview(stack19)
        stack19.addArrangedSubview(lblIngredient19)
        stack19.addArrangedSubview(lblMeasure19)
        //2
        bgView.addSubview(stack20)
        stack20.addArrangedSubview(lblIngredient20)
        stack20.addArrangedSubview(lblMeasure20)
        
        setupContentView()
        
    }
    
    fileprivate func setupContentView() {
        
        let detailViews = ["bgView":bgView, "title":title, "stack1":stack1,"stack2":stack2,"stack3":stack3,"stack4":stack4,"stack5":stack5,"stack6":stack6,"stack7":stack7,"stack8":stack8,"stack9":stack9,"stack10":stack10,"stack11":stack11,"stack12":stack12,"stack13":stack13,"stack14":stack14,"stack15":stack15,"stack16":stack16,"stack17":stack17,"stack18":stack18,"stack19":stack19,"stack20":stack20 ]
        
        //background
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[bgView]-10-|", options: [], metrics: nil, views: detailViews))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[bgView]-10-|", options: [], metrics: nil, views: detailViews))
        
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[title]-8-|", options: [], metrics: nil, views: detailViews))
        
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack1]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack2]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack3]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack4]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack5]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack6]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack7]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack8]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack9]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack10]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack11]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack12]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack13]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack14]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack15]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack16]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack17]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack18]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack19]-8-|", options: [], metrics: nil, views: detailViews))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[stack20]-8-|", options: [], metrics: nil, views: detailViews))
        
        //constraint vertical
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[title(25)]-8-[stack1]-8-[stack2]-8-[stack3]-8-[stack4]-8-[stack5]-8-[stack6]-8-[stack7]-8-[stack8]-8-[stack9]-8-[stack10]-8-[stack11]-8-[stack12]-8-[stack13]-8-[stack14]-8-[stack15]-8-[stack16]-8-[stack17]-8-[stack18]-8-[stack19]-8-[stack20]|", options: [], metrics: nil, views: detailViews))
    }
    
    func setDataIngredient(strIngredient1 : String?, strMeasure1:String?,
                           strIngredient2 : String?, strMeasure2:String?,
                           strIngredient3 : String?, strMeasure3:String?,
                           strIngredient4 : String?, strMeasure4:String?,
                           strIngredient5 : String?, strMeasure5:String?,
                           strIngredient6 : String?, strMeasure6:String?,
                           strIngredient7 : String?, strMeasure7:String?,
                           strIngredient8 : String?, strMeasure8:String?,
                           strIngredient9 : String?, strMeasure9:String?,
                           strIngredient10 : String?, strMeasure10:String?,
                           strIngredient11 : String?, strMeasure11:String?,
                           strIngredient12 : String?, strMeasure12:String?,
                           strIngredient13 : String?, strMeasure13:String?,
                           strIngredient14 : String?, strMeasure14:String?,
                           strIngredient15 : String?, strMeasure15:String?,
                           strIngredient16 : String?, strMeasure16:String?,
                           strIngredient17 : String?, strMeasure17:String?,
                           strIngredient18 : String?, strMeasure18:String?,
                           strIngredient19 : String?, strMeasure19:String?,
                           strIngredient20 : String?, strMeasure20:String?) {
        
        if strIngredient1 == "" || strMeasure1 == ""{
            lblIngredient1.text = nil
            lblMeasure1.text = nil
        }else{
            lblIngredient1.text = strIngredient1
            lblMeasure1.text = strMeasure1
        }
        
        if strIngredient2 == "" || strMeasure2 == ""{
            lblIngredient2.text = nil
            lblMeasure2.text = nil
        }else{
            lblIngredient2.text = strIngredient2
            lblMeasure2.text = strMeasure2
        }
    
        if strIngredient3 == "" || strMeasure3 == ""{
            lblIngredient3.text = nil
            lblMeasure3.text = nil
        }else{
            lblIngredient3.text = strIngredient3
            lblMeasure3.text = strMeasure3
        }

        if strIngredient4 == "" || strMeasure4 == ""{
            lblIngredient4.text = nil
            lblMeasure4.text = nil
        }else{
            lblIngredient4.text = strIngredient4
            lblMeasure4.text = strMeasure4
        }
        
        if strIngredient5 == "" || strMeasure5 == ""{
            lblIngredient5.text = nil
            lblMeasure5.text = nil
        }else{
            lblIngredient5.text = strIngredient5
            lblMeasure5.text = strMeasure5
        }
        
        if strIngredient6 == "" || strMeasure6 == ""{
            lblIngredient6.text = nil
            lblMeasure6.text = nil
        }else{
            lblIngredient6.text = strIngredient6
            lblMeasure6.text = strMeasure6
        }
        
        if strIngredient7 == "" || strMeasure7 == ""{
            lblIngredient7.text = nil
            lblMeasure7.text = nil
        }else{
            lblIngredient7.text = strIngredient7
            lblMeasure7.text = strMeasure7
        }
        
        if strIngredient8 == "" || strMeasure8 == ""{
            lblIngredient8.text = nil
            lblMeasure8.text = nil
        }else{
            lblIngredient8.text = strIngredient8
            lblMeasure8.text = strMeasure8
        }
        
        if strIngredient9 == "" || strMeasure9 == ""{
            lblIngredient9.text = nil
            lblMeasure9.text = nil
        }else{
            lblIngredient9.text = strIngredient9
            lblMeasure9.text = strMeasure9
        }
        
        if strIngredient10 == "" || strMeasure10 == ""{
            lblIngredient10.text = nil
            lblMeasure10.text = nil
        }else{
            lblIngredient10.text = strIngredient10
            lblMeasure10.text = strMeasure10
        }
        
        if strIngredient11 == "" || strMeasure11 == ""{
            lblIngredient11.text = nil
            lblMeasure11.text = nil
        }else{
            lblIngredient11.text = strIngredient11
            lblMeasure11.text = strMeasure11
        }
        
        if strIngredient12 == "" || strMeasure12 == ""{
            lblIngredient12.text = nil
            lblMeasure12.text = nil
        }else{
            lblIngredient12.text = strIngredient12
            lblMeasure12.text = strMeasure12
        }
        
        if strIngredient13 == "" || strMeasure13 == ""{
            lblIngredient13.text = nil
            lblMeasure13.text = nil
        }else{
            lblIngredient13.text = strIngredient13
            lblMeasure13.text = strMeasure13
        }
        
        if strIngredient14 == "" || strMeasure14 == ""{
            lblIngredient14.text = nil
            lblMeasure14.text = nil
        }else{
            lblIngredient14.text = strIngredient14
            lblMeasure14.text = strMeasure14
        }
        
        if strIngredient15 == "" || strMeasure15 == ""{
            lblIngredient15.text = nil
            lblMeasure15.text = nil
        }else{
            lblIngredient15.text = strIngredient15
            lblMeasure15.text = strMeasure15
        }
      
        if strIngredient16 == "" || strMeasure16 == ""{
            lblIngredient16.text = nil
            lblMeasure16.text = nil
        }else{
            lblIngredient16.text = strIngredient16
            lblMeasure16.text = strMeasure16
        }
        
        if strIngredient17 == "" || strMeasure17 == ""{
            lblIngredient17.text = nil
            lblMeasure17.text = nil
        }else{
            lblIngredient17.text = strIngredient17
            lblMeasure17.text = strMeasure17
        }
        
        if strIngredient18 == "" || strIngredient18 == ""{
            lblIngredient18.text = nil
            lblMeasure18.text = nil
        }else{
            lblIngredient18.text = strIngredient18
            lblMeasure18.text = strMeasure18
        }
        
        if strMeasure19 == "" || strMeasure19 == ""{
            lblIngredient19.text = nil
            lblMeasure19.text = nil
        }else{
            lblIngredient19.text = strIngredient19
            lblMeasure19.text = strMeasure19
        }
        
        if strIngredient20 == "" || strMeasure20 == ""{
            lblIngredient20.text = nil
            lblMeasure20.text = nil
        }else {
            lblIngredient20.text = strIngredient20
            lblMeasure20.text = strMeasure20
        }
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
