//
//  HeaderView.swift
//  CockBook
//
//  Created by Tinh Van on 4/11/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import SDWebImage

class HeaderView: UITableViewCell {
    
    let bgView :UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    let imageHeader :UIImageView = {
        let img = UIImageView()
        img.image = R.image.lime()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let views = ["bgView":bgView , "imageHeader":imageHeader]
        contentView.addSubview(bgView)
        bgView.addSubview(imageHeader)
        
        imageHeader.circleImage()
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[bgView]|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[bgView]|", options: [], metrics: nil, views: views))
        
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imageHeader]-10-|", options: [], metrics: nil, views: views))
        bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[imageHeader(350)]-10-|", options: [], metrics: nil, views: views))
        
    }
    
    func setImageForHeader(imgHeaderLink:String){
        imageHeader.sd_setImage(with: URL(string: imgHeaderLink))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
