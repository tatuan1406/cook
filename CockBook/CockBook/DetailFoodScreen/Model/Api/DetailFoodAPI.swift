//
//  DetailFoodAPI.swift
//  CockBook
//
//  Created by Tinh Van on 4/11/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya

enum DetailFoodApi{
    case foodKey(nameOfFood: String)
}
extension DetailFoodApi: TargetType{
    var baseURL: URL {
        return URL(string: baseUrl)!
    }
    
    var path: String {
        switch self {
        case .foodKey(_):
            return "api/json/v1/1/search.php"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .foodKey(_):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .foodKey(let nameOfFood):
            return .requestParameters(parameters: ["s" : nameOfFood], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
