//
//  MainAPI.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya

//https://www.themealdb.com/api/json/v1/1/random.php

enum MainAPI{
    case randomKeyword()
    case categoryKeyword()
}

extension MainAPI:TargetType{
    
    var baseURL: URL {
        return URL(string: baseUrl)!
    }
    
    var path: String {
        switch self {
        case .randomKeyword():
            return "api/json/v1/1/random.php"
        case .categoryKeyword():
            return "api/json/v1/1/categories.php"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .randomKeyword():
            return .get
        case .categoryKeyword():
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .randomKeyword(_):
            return .requestParameters(parameters: ["":""], encoding: URLEncoding.default)
        case .categoryKeyword(_):
            return .requestParameters( parameters: ["":""], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
