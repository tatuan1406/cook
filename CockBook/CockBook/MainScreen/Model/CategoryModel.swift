//
//  CategoryModel.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ModelCategories : Mappable {
    var categories : [Categories]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        categories <- map["categories"]
    }
    
}
struct Categories : Mappable {
    var idCategory : String?
    var strCategory : String?
    var strCategoryThumb : String?
    var strCategoryDescription : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        idCategory <- map["idCategory"]
        strCategory <- map["strCategory"]
        strCategoryThumb <- map["strCategoryThumb"]
        strCategoryDescription <- map["strCategoryDescription"]
    }
    
}

