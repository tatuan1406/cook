//
//  PresenterMain.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

protocol DelegateMain:class {
    func didGetData()
}
class PresenterlMain {
    weak var delegateMain : DelegateMain?
    
    convenience init(delegate: DelegateMain){
        self.init()
        self.delegateMain = delegate
    }
    
    let mainProvider = MoyaProvider<MainAPI>()
    
    var randomModel : ModelRandomToday?
    
    var categoryModel : ModelCategories?
    
    func getDataForRandomToday(){
        mainProvider.request(MainAPI.randomKeyword()) { (result) in
            switch result{
            case .success(let response):
                do{
                    let json = try response.mapJSON()
                    guard let dataJSON = Mapper<ModelRandomToday>().map(JSONObject:json)
                        else {return}
                    self.randomModel = dataJSON
                    self.delegateMain?.didGetData()
                    
                } catch {
                    print("error")
                }
            case .failure(_):
                print("error")
            }
        }
        
    }
    
    func getDataForCategory(){
        mainProvider.request(MainAPI.categoryKeyword()) { (resultCategory) in
            switch resultCategory{
            case .success(let response):
                do{
                    let json = try response.mapJSON()
                    guard let dataJSON = Mapper<ModelCategories>().map(JSONObject:json)
                        else {return}
                    self.categoryModel = dataJSON
                    self.delegateMain?.didGetData()
                } catch {
                    print("error")
                }
            case .failure(_):
                print("error")
            }
        }
    }
}
