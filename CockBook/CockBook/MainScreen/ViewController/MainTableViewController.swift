//
//  ViewController.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class MainTableViewController: UICollectionViewController , UICollectionViewDelegateFlowLayout{
    
    private let presenterMain = PresenterlMain()
    
    private var arrCategoryLink:[String] = []
    private var arrCategoryName:[String] = []
    private var arrAreaName:[String] = []
    
     var random:String?
     var category:String?
     var navTitile:String?
    var btnLang:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = UIColor.white
//        LanguageFile.lang = "en"
        setText()
        navigationStyle()
        registerNib()
        
        presenterMain.delegateMain = self
        presenterMain.getDataForRandomToday()
        presenterMain.getDataForCategory()
    }
    
    private func setText(){
        let languageFile = LanguageFile()
        random = languageFile.random
        category = languageFile.category
        navTitile  = languageFile.cooking
        btnLang = languageFile.language
    }
    
    private func navigationStyle(){
        navigationItem.title = navTitile
        navigationController?.navigationBar.barTintColor = UIColor.deepGreen
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: btnLang, style: .plain, target: self, action: #selector (choseLanguage))
    }
    
    @objc private func choseLanguage()  {
        actionSheet()
    }
    
    func actionSheet(){
        let actionSheet = UIAlertController(title: "Choose your Language", message: nil, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let englishLang = UIAlertAction(title: "English", style: .default) { (action) in
            LanguageFile.lang = "en"
          UserDefaults.standard.set("en", forKey: "lang")
            self.viewDidLoad()
        }
        let japanLang = UIAlertAction(title: "Japan", style: .default) { (action) in
            LanguageFile.lang = "ja"
            UserDefaults.standard.set("ja", forKey: "lang")
            self.viewDidLoad()
        }
        let vietLang = UIAlertAction(title: "Viet Nam", style: .default) { (action) in
            LanguageFile.lang = "vi-VN"
            UserDefaults.standard.set("vi-VN", forKey: "lang")
            self.viewDidLoad()
        }
        actionSheet.addAction(englishLang)
        actionSheet.addAction(japanLang)
        actionSheet.addAction(vietLang)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    private  func registerNib() {
        
        collectionView.register(RandomCollectionViewCell.self, forCellWithReuseIdentifier: "RandomTVC")
        
        collectionView.register(CollectionTVC.self, forCellWithReuseIdentifier: "CollectionTVC")
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            //Random Cell
            let randomCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RandomTVC", for: indexPath) as! RandomCollectionViewCell
            randomCell.setDataForRandom(section: random ?? "fail value random",
                                        image: presenterMain.randomModel?.meals?[indexPath.row].strMealThumb,
                                        name: presenterMain.randomModel?.meals?[indexPath.row].strMeal,
                                        detail: presenterMain.randomModel?.meals?[indexPath.row].strInstructions)
            return randomCell
        }
        
        //Category Cell
        let categoryCount = presenterMain.categoryModel?.categories?.count ?? 0
        for i in 0..<categoryCount{
            arrCategoryLink.append(presenterMain.categoryModel?.categories?[i].strCategoryThumb ?? "")
            arrCategoryName.append(presenterMain.categoryModel?.categories?[i].strCategory ?? "")
        }
        let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionTVC", for: indexPath) as! CollectionTVC
        categoryCell.setDataForCollectionItem(strSection: category ?? "fail value category",
                                              intCount: categoryCount,
                                              strImgLink: arrCategoryLink,
                                              strItemName: arrCategoryName)
        return categoryCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
            return CGSize.init(width: collectionView.bounds.width, height: 200)
        }
        return CGSize.init(width: collectionView.bounds.width, height: 230)
        
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
//            let layout = UICollectionViewFlowLayout()
            let detailFoodVC = FoodDetailTableViewController()
            detailFoodVC.getFoodName(foodName: presenterMain.randomModel?.meals?[indexPath.row].strMeal ?? "")
            self.navigationController?.pushViewController(detailFoodVC, animated: true)
            
        }
    }
}

extension MainTableViewController: DelegateMain {
    func didGetData() {
        collectionView.reloadData()
    }
}
