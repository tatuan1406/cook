//
//  CollectionTVC.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

class CollectionTVC: UICollectionViewCell {
    
    let collection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let lbl_section : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment =  NSTextAlignment.right
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 21)
        lbl.textColor = UIColor.deepGreen
        return lbl
    }()
    
    var imgLink:[String] = []
    var itemName:[String] = []
    var itemCount:Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionViewSetup()
        setupView()
    }
    
    func collectionViewSetup(){
        collection.allowsSelection = true
        
        self.collection.delegate = self
        self.collection.dataSource = self
        
        collection.register(ItemCollectionViewCell.self, forCellWithReuseIdentifier: "ItemCollectionViewCell")
    }
    
    
    func setDataForCollectionItem(strSection:String,intCount: Int, strImgLink:[String], strItemName: [String]) {
        lbl_section.text = strSection
        itemCount = intCount
        imgLink = strImgLink
        itemName = strItemName
        
        collection.reloadData()
    }
    func setupView() {
        
        contentView.addSubview(lbl_section)
        contentView.addSubview(collection)
        
        let towViews = ["lbl_section" : lbl_section, "collection" : collection]
        let lblMetrics = ["lblHeight": 22 ]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[lbl_section]-8-|", options: [], metrics: nil, views: towViews))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[lbl_section(lblHeight@999)]-16-[collection]-16-|", options: [], metrics: lblMetrics, views: towViews))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[collection]-8-|", options: [], metrics: nil, views: towViews))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CollectionTVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collection.dequeueReusableCell(withReuseIdentifier: "ItemCollectionViewCell", for: indexPath) as! ItemCollectionViewCell
        collectionCell.setDataItemCollection(img: imgLink[indexPath.row], name: itemName[indexPath.row] )
        
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let layout = UICollectionViewFlowLayout()
        let fir = CategoriesCollectionViewController(collectionViewLayout: layout )
        fir.getKey(keyCategory: itemName[indexPath.row])
       (self.window?.rootViewController as? UINavigationController)?.pushViewController(fir, animated: true)
    }
}
