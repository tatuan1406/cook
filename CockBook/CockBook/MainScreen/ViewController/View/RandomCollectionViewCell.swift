//
//  RandomTableViewCell.swift
//  CockBook
//
//  Created by Ta Tuan Macbook on 3/26/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit
import SDWebImage

class RandomCollectionViewCell: UICollectionViewCell {

    let lbl_section : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment =  NSTextAlignment.right
        lbl.font = UIFont(name: "HelveticaNeue-Bold", size: 21)
        lbl.textColor = UIColor.deepGreen
        return lbl
    }()
    
    let view1 : UIView = {
        let myView = UIView()
        myView.backgroundColor = UIColor.white
        myView.translatesAutoresizingMaskIntoConstraints = false
        myView.cardView()
        return myView
    }()
    
    let img_random : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.borderImage()
        img.widthAnchor.constraint(equalTo: img.heightAnchor, multiplier: 1).isActive = true
        return img
    }()
    
    let stackview1 : UIStackView = {
        let stv = UIStackView()
        stv.translatesAutoresizingMaskIntoConstraints = false
        stv.axis = .vertical
        stv.alignment = .fill
        stv.distribution = .fillEqually
        return stv
    }()
    
    let lbl_name: UILabel = {
        let lblTitle = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .center
        lblTitle.textColor = UIColor.deepRed
        return lblTitle
    }()
    
    let lbl_detail: UILabel = {
        let lblSub = UILabel()
        lblSub.translatesAutoresizingMaskIntoConstraints = false
        lblSub.numberOfLines = 2
          lblSub.textAlignment = .justified
        return lblSub
    }()


    override init(frame: CGRect) {
        super.init(frame: frame)
        mainCollectionViewCellSetUp()
    }

    func setDataForRandom(section: String,image :String? , name: String?, detail:String?){
        lbl_section.text = NSLocalizedString(section, comment: "")
        img_random.sd_setImage(with: URL(string: image ?? "unknowLink"))
        lbl_name.text = name ?? ""
        lbl_detail.text = detail ?? ""
    }
    
    func mainCollectionViewCellSetUp(){
        contentView.addSubview(lbl_section)
        contentView.addSubview(view1)
        
        view1.addSubview(img_random)
        view1.cardView()
        view1.addSubview(stackview1)
        stackview1.addArrangedSubview(lbl_name)
        stackview1.addArrangedSubview(lbl_detail)
        
        let views = ["lbl_section" : lbl_section, "view1" : view1, "img_random" : img_random, "stackview1": stackview1, "lbl_name" : lbl_name , "lbl_detail" : lbl_detail]
        
        //add view1 to view and constraint
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[view1]-8-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[lbl_section]-8-|", options: [], metrics: nil, views: views))
        
        let lblMetrics = ["lblHeight": 24 ]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-6-[lbl_section(lblHeight@999)]-16-[view1]-16-|", options: [], metrics: lblMetrics, views: views))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[img_random]-16-[stackview1]-16-|", options: [], metrics: nil, views: views))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[img_random]-8-|", options: [], metrics: nil, views: views))
        
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[stackview1]-8-|", options: [], metrics: nil, views: views))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
