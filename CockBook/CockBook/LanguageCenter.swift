//
//  StringExtension.swift
//  CockBook
//
//  Created by Tinh Van on 4/9/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation

class LanguageFile: NSObject {
    
    static var lang:String = UserDefaults.standard.string(forKey: "lang") ?? ""
    
    //Get language based string
    var cooking = "cooking".localizationChange(localization:LanguageFile.lang )
    var random = "random".localizationChange(localization:LanguageFile.lang )
    var category = "category".localizationChange(localization:LanguageFile.lang )
    var detail = "detail".localizationChange(localization:LanguageFile.lang )
    var ingredient = "ingredient".localizationChange(localization:LanguageFile.lang )
    var measure = "measure".localizationChange(localization:LanguageFile.lang )
    var video = "video".localizationChange(localization:LanguageFile.lang )
    var language = "language".localizationChange(localization: LanguageFile.lang )
}

extension String{
    func localizationChange(localization:String) -> String{
        let path = Bundle.main.path(forResource: localization, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
