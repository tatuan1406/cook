//
//  CategoryFoodCell.swift
//  CockBook
//
//  Created by Tinh Van on 4/11/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import SDWebImage

class CategoryFoodCell: UICollectionViewCell{
    
    let item_view : UIView = {
        let myView = UIView()
        myView.backgroundColor = UIColor.white
        myView.translatesAutoresizingMaskIntoConstraints = false
        myView.cardView()
        return myView
    }()
    let item_img : UIImageView = {
        let img = UIImageView()
        
        img.translatesAutoresizingMaskIntoConstraints = false
        img.borderImage()
        img.widthAnchor.constraint(equalTo: img.heightAnchor, multiplier: 1).isActive = true
        return img
    }()
    
    let item_lblName : UILabel = {
        let lblTitle = UILabel()
        lblTitle.textColor = UIColor.deepRed
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font = UIFont(name: "HelveticaNeue", size: 18)
        lblTitle.numberOfLines = 2
        lblTitle.textAlignment = .center
        return lblTitle
    }()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setupItemView()
    }
    
    func setupItemView() {
        
        contentView.addSubview(item_view)
        
        item_view.addSubview(item_img)
        item_view.addSubview(item_lblName)
        
        //dictionary of view
        let views = ["item_view" : item_view, "item_img" : item_img, "item_lblName" : item_lblName]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[item_view]-8-|", options: [], metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[item_view]-8-|", options: [], metrics: nil, views: views))
        
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[item_img]-8-[item_lblName]-8-|", options: [], metrics: nil, views: views))
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[item_img]-8-|", options: [], metrics: nil, views: views))
        item_view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[item_lblName]-8-|", options: [], metrics: nil, views: views))
    }
    
    func setDataForCategoryFood(img: String?, name: String?) {
        item_img.sd_setImage(with: URL(string: img ?? "unknowLink" ))
        item_lblName.text = name
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
