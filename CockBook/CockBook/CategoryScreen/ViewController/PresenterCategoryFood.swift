//
//  PresenterCategoryFood.swift
//  CockBook
//
//  Created by Tinh Van on 4/10/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper

protocol DelegateCategoryFood: class {
    func getData()
}

class PresenterCategoryFood{
    weak var delegateCategoryFood: DelegateCategoryFood?
    
    convenience init(delegate: DelegateCategoryFood)
    {
        self.init()
        self.delegateCategoryFood = delegate
    }
    
    var keyCate:String?
    func getKeyCategory(key:String){
        self.keyCate = key
    }
    let categoryFoodProvider = MoyaProvider<CategoryFoodApi>()
    
    var categoryFoodModel : ModelCategoryFood?
    
    func getDataForCategoryFood()  {
        categoryFoodProvider.request(CategoryFoodApi.foodKey(key: keyCate ?? "")) { (result) in
            switch result{
            case .success(let response):
                do{
                    let json = try response.mapJSON()
                    guard let dataJSON = Mapper<ModelCategoryFood>().map(JSONObject: json)
                        else {
                            return
                    }
                    self.categoryFoodModel = dataJSON
                    self.delegateCategoryFood?.getData()
                }catch{
                    print("error ")
                }
            case .failure(_):
                print("error ")
            }
        }
    }
    
}
