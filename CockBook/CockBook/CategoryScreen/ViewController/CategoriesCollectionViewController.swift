//
//  CategoriesCollectionViewController.swift
//  CockBook
//
//  Created by Tinh Van on 4/10/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import UIKit

private let reuseIdentifier = "categoryFoodCell"

class CategoriesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    private  let presenterCategoryFood = PresenterCategoryFood()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = UIColor.white
        
        presenterCategoryFood.delegateCategoryFood = self
        
        presenterCategoryFood.getDataForCategoryFood()
        
        navigationBar()
        
        collectionViewSetup()
        
       //refresh control
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector (CategoriesCollectionViewController.refreshControl), for: .valueChanged)
    }
    
    @objc func refreshControl()  {
        collectionView.reloadData()
        collectionView.refreshControl?.endRefreshing()
    }
    
    @objc func Categories()  {
        
    }
    
    func navigationBar()  {
        navigationItem.title = presenterCategoryFood.keyCate?.uppercased()
        navigationController?.navigationBar.barTintColor = UIColor.deepGreen
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Categories", style: .plain, target: self, action: #selector (Categories))
    }
    func collectionViewSetup()  {
        collectionView?.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collectionView.delegate = self
        collectionView.dataSource = self
        self.collectionView!.register(CategoryFoodCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    func getKey(keyCategory:String){
        presenterCategoryFood.getKeyCategory(key: keyCategory)
        print(keyCategory)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenterCategoryFood.categoryFoodModel?.meals?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoryFoodCell
        cell.setDataForCategoryFood(img: presenterCategoryFood.categoryFoodModel?.meals?[indexPath.row].strMealThumb,
                                                            name: presenterCategoryFood.categoryFoodModel?.meals?[indexPath.row].strMeal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfColume:CGFloat = 2
        let width = collectionView.frame.size.width
        let xInset :CGFloat = 10
        let cellSpacing :CGFloat = 5
        return CGSize(width: (width / numberOfColume) - (xInset + cellSpacing) , height: ((width / numberOfColume) - (xInset + cellSpacing ) ) + 50  )
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//         let layout = UICollectionViewFlowLayout()
        let detailFoodVC = FoodDetailTableViewController()
        detailFoodVC.getFoodName(foodName: presenterCategoryFood.categoryFoodModel?.meals?[indexPath.row].strMeal ?? "nameFood")
       self.navigationController?.pushViewController(detailFoodVC, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let roationTransform = CATransform3DTranslate(CATransform3DIdentity, -30, 10, 0)
        cell.alpha = 0
        cell.layer.transform = roationTransform
        UIView.animate(withDuration: 0.75) {
            cell.layer.transform = CATransform3DIdentity
            cell.alpha = 1
        }
    }

}

extension CategoriesCollectionViewController: DelegateCategoryFood
{
    func getData() {
        collectionView.reloadData()
    }
}

