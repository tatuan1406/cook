//
//  CategoryFoodAPI.swift
//  CockBook
//
//  Created by Tinh Van on 4/10/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import Moya

enum CategoryFoodApi {
    case foodKey(key: String)
}
extension CategoryFoodApi:TargetType{
    var baseURL: URL {
        return URL(string: baseUrl)!
    }
    
    var path: String {
        switch self {
        case .foodKey(_):
            return "api/json/v1/1/filter.php"
        }
    }
    var method: Moya.Method {
        switch self {
        case .foodKey(_):
            return .get
        }
    }
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .foodKey(let category):
            return .requestParameters(parameters: ["c" : category], encoding: URLEncoding.default)
        }
    }
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
