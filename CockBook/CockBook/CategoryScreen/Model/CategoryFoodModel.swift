//
//  CategoryFoodModel.swift
//  CockBook
//
//  Created by Tinh Van on 4/10/19.
//  Copyright © 2019 HiUPTeam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ModelCategoryFood : Mappable {
    var meals : [Meals]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        meals <- map["meals"]
    }
    
}
struct Meals : Mappable {
    var strMeal : String?
    var strMealThumb : String?
    var idMeal : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        strMeal <- map["strMeal"]
        strMealThumb <- map["strMealThumb"]
        idMeal <- map["idMeal"]
    }
    
}
